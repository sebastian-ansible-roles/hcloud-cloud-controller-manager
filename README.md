# ansible Role: hcloud cloud controller manager

This ansible role deploys the Hetzner Cloud [hcloud cloud controller manager](https://github.com/hetznercloud/hcloud-cloud-controller-manager) to a kubernetes cluster.

## Requirements

* Hetzner Cloud API Token
* A running kubernetes cluster on [Hetzner Cloud](https://www.hetzner.com/de/cloud)

## Role Variables

* hcloud_api_token
* k8s_kubeconfig (path to kubeconfig)

## Dependencies

* none

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
- hosts: localhost
  gather_facts: no
  collections:
    - community.kubernetes.k8s
  roles: 
    - role: hcloud-cloud-controller-manager
      tags: hcloud-ccm
```

## License

BSD

## Author Information

Sebastian Schmid (@sebastian.schmid)
